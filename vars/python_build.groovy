def call(dockerRepoName, imageName, portNum) {
    pipeline {
        agent any
        parameters {
            booleanParam(defaultValue: false, description: 'Deploy the App', name: 'DEPLOY')
        }
        stages {
            stage('Lint') {
                steps {
                    sh 'pylint-fail-under --fail_under 5 *.py'
                }
            }
            stage('Package') {
                steps {
                    withCredentials([string(credentialsId: 'mahsataer', variable: 'TOKEN')]) {
                        sh "docker login -u 'mahsataer' -p '$TOKEN' docker.io"
                        sh "docker build -t ${dockerRepoName}:latest --tag mahsataer/${dockerRepoName}:${imageName} ."
                        sh "docker push mahsataer/${dockerRepoName}:${imageName}"
                    }
                }
            }
            stage('Info') {
                steps {
                    withCredentials([string(credentialsId: 'mahsataer', variable: 'TOKEN')]) {
                        sh "docker login -u 'mahsataer' -p '$TOKEN' docker.io"
                        sh "docker inspect mahsataer/${dockerRepoName}:${imageName} > info.txt"
                    }
                }
            }
            stage('Zip Artifacts') {
                steps {
                    sh 'zip app.zip *.*'
                    archiveArtifacts artifacts: 'app.zip'
                }
            }
            stage('Deliver') {
                when {
                    expression { params.DEPLOY }
                }
                steps {
                    sh "docker stop ${dockerRepoName}:latest || true && docker rm ${dockerRepoName}:latest || true"
                    sh "docker run -d -p ${portNum}:${portNum} --name ${dockerRepoName} ${dockerRepoName}:latest || true"
                    sh "docker ps"
                }
            }
        }
    }
}
